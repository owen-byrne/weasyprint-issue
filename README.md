### Installation

```
pipenv install
pipenv shell
```

### Running the tests

```
time weasyprint <html-file> weasy-test.pdf
```
